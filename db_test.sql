-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 26 Agu 2020 pada 11.06
-- Versi Server: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_test`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_key`
--

CREATE TABLE IF NOT EXISTS `tb_key` (
  `id` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `key_kunci` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_key`
--

INSERT INTO `tb_key` (`id`, `username`, `key_kunci`) VALUES
(2, 'jodyravi', 'YINvYV/fYOZ0V88V'),
(3, 'kalongtaro', 'fQbT6/mmUBUCKqMI'),
(4, 'rumah_kita', '+/O/geYU0tnna7RQ'),
(5, 'mumamu', '??|?;6??g??~'),
(6, 'burik', '?????2??6??'),
(7, 'berak', '?@4??&|!y>t%?\\=?');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id` int(10) NOT NULL,
  `userid` varchar(40) DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `userid`, `username`, `password`) VALUES
(1, '', 'jodravi', 'asfw'),
(2, '', 'thanks you', 'YXNmdw=='),
(3, NULL, 'Your_Head', '175ceabc3d2ec00264d47493d33b131ea1d11647a1786329a61d2a438b40e984874770f102e6193973b2f55809395412e36b7abe84568103b134e8c5e80e1927'),
(4, NULL, 'blankon', 'd37a1660533899e767a83636506bd63cb1093337fa8e591fd918587442c5121549bf9bc7a7e4cc5f6db580303f9094447689af4a2f27e0aaeffe24a07da52c71'),
(5, NULL, 'lilian', 'h+gMWwBRjk5HoLSZRJYDUQ=='),
(6, NULL, 'jodyravi', 'TNwHwQC98JUryt9J486T/A=='),
(7, NULL, 'kalongtaro', 'REArCWHLw/EJ+vTGnZ8K9Z3pqSLUimWudu28zOA+xwE='),
(8, NULL, 'rumah_kita', 'JWnor6ZpIyG5wFWI3gADCg=='),
(9, NULL, 'mumamu', 'dz5j7jvZ/GcQ2PamroIT2g=='),
(10, NULL, 'burik', 'UvoHWAo+KsymDCod8C3x8Q=='),
(11, NULL, 'berak', 'PSs7K9bGDlvRrRVzx8QVtA==');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_key`
--
ALTER TABLE `tb_key`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_key`
--
ALTER TABLE `tb_key`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_key`
--
ALTER TABLE `tb_key`
ADD CONSTRAINT `tb_key_ibfk_1` FOREIGN KEY (`username`) REFERENCES `tb_user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
