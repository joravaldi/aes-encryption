package com.myproject.Hello.Repository;

import com.myproject.Hello.Models.Data.User;
import org.jboss.logging.Logger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Integer> {
    Logger logger = Logger.getLogger(UserRepository.class.getName());
    @Override
    List<User> findAll();
    List<User> findByuserId (String userId);

    User findByUsernameAndPassword(String username,String password);

    User findByUsername(String username);

    @Override
    <S extends User> S save(S s);
}
