package com.myproject.Hello.Repository;

import com.myproject.Hello.Models.Data.Key;
import org.jboss.logging.Logger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KeyRepository extends JpaRepository<Key,Integer> {
    Logger logger = Logger.getLogger(KeyRepository.class.getName());

    Key findByUsername(String username);

    @Override
    <S extends Key> S save(S s);
}
