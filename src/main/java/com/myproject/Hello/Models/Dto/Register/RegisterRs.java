package com.myproject.Hello.Models.Dto.Register;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class RegisterRs implements Serializable {
    private final static String serialVersionUUID = UUID.randomUUID().toString();

    private String code, description;

    public static String getSerialVersionUUID() {
        return serialVersionUUID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
