package com.myproject.Hello.Models.Dto.Register;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;



public class RegisterRq implements Serializable {
    private static final long serialVersionUID = 2994635383970531826L;
    private String username;
    private String toUsername;
    private String password;
    private String toPassword;



    public RegisterRq() {
    }

    public String getToUsername() {
        return toUsername;
    }

    public void setToUsername(String toUsername) {
        this.toUsername = toUsername;
    }

    public String getToPassword() {
        return toPassword;
    }

    public void setToPassword(String toPassword) {
        this.toPassword = toPassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
