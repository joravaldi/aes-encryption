package com.myproject.Hello.Models.Data;

import com.myproject.Hello.Config.Generator.GenerateUserId;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Data
@Table(name = "tb_user")
public class User implements Serializable {
    @Id
//    @SequenceGenerator(name = "SEQ_TABLE_USER_IDX",sequenceName ="SEQ_TABLE_USER_IDX",allocationSize = 1)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "SEQ_TABLE_USER_IDX")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 10, nullable = false)
    private  int id;


    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userid")
    @GenericGenerator(name = "userid", strategy = "com.example.CRUD.Config.Generator.GenerateUserId",
            parameters = {
                    @org.hibernate.annotations.Parameter( name = GenerateUserId.INCREMENT_PARAM, value = "20"),
                    @org.hibernate.annotations.Parameter(name = GenerateUserId.VALUE_PREFIX_PARAMETER,value = "USR_"),
                    @org.hibernate.annotations.Parameter(name = GenerateUserId.NUMBER_FORMAT_PARAMETER,value = "%05d")
            })
    @Column(name = "userid", length = 40,nullable = true)
    private String userId;

    @Column(name = "username", length = 30, nullable = false)
    private  String username;

    @Column(name = "password", length = 300,nullable = false)
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
