package com.myproject.Hello.Service;

import com.myproject.Hello.Repository.KeyRepository;
import com.sun.xml.messaging.saaj.packaging.mime.util.BASE64EncoderStream;
import org.jboss.logging.Logger;
import org.jvnet.staxex.Base64EncoderStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.security.Key;
import java.util.Base64;
import java.util.Random;

public class AES {
    Logger logger = Logger.getLogger(AES.class.getName());


    private static final String ALGO = "AES";
    @Autowired
    KeyRepository keyJPA;

    private String username;
    private byte[] keyValue;
    public AES (String key){
        this.keyValue = key.getBytes();
    }


    private Key generateKey()throws SecurityException{
        logger.info("### Start EXCUTE Generate Key");
        Key key = null;
        try {
             key = new SecretKeySpec(keyValue,ALGO);
             logger.info("GENERATE KEY VALUE : " + key.toString());
            return  key;
        }catch (SecurityException e){
            logger.error("error generate Key : "+ e);
            return null;
        }

    }
    public String encrypt (String password)throws Exception{
        logger.info("### Start Excute Encrypt");
        String encryptedValue =  null;
        try {
            Key key = generateKey();
            logger.info("KEY :" + key );
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.ENCRYPT_MODE, key);
            byte [] encryptVal = c.doFinal(password.getBytes());
            encryptedValue = Base64Utils.encodeToString(encryptVal);
            logger.info("result encrypt : " + encryptedValue);
            return encryptedValue;
        }catch (Exception e )
        {
            logger.error("###Error Encrypt data cause : " + e);
        }

        return encryptedValue;
    }
    public String decrypt (String encryptedData) throws  Exception{
        logger.info("### DECRYPT DATA : " + encryptedData);
        Key key = generateKey();
        logger.info("KEY : " + key.toString());
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE,key);
        byte[] decorderVal = Base64Utils.decodeFromString(encryptedData);
        String valueDecrypt = c.doFinal(decorderVal).toString();
        logger.info("RESULT VALUE DECRYPT : " + valueDecrypt);
        return valueDecrypt;
    }



}
