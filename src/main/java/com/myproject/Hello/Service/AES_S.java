package com.myproject.Hello.Service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class AES_S {

    private static final  Logger logger = Logger.getLogger(AES_S.class);
    private  static final String UNICODE_FORMATE = "UTF-8";
    private static final String EncryptionType = "AES";



    public static String GenerateKey() throws NoSuchAlgorithmException {
      try{
          KeyGenerator keyGenerator = KeyGenerator.getInstance(EncryptionType );
          SecretKey  key = keyGenerator.generateKey();
          String encodeKey = Base64Utils.encodeToString(key.getEncoded());
          return encodeKey;

      }catch (NoSuchAlgorithmException e){
          logger.info("####GenerateKey Exception : "+ e);
          return  null;
      }
    }
    public  String Encrypt (String dataToEncrypt,String key)  {
      try{

          byte[] decodeKey = Base64Utils.decodeFromString(key);
          SecretKey secretKey = new SecretKeySpec(decodeKey,0,decodeKey.length,EncryptionType);
          byte[] data = dataToEncrypt.getBytes(UNICODE_FORMATE);
          Cipher cipher = Cipher.getInstance(EncryptionType);
          cipher.init(Cipher.ENCRYPT_MODE,secretKey);
          byte[] encrypt = cipher.doFinal(data);
          String encodeEncrypt = Base64Utils.encodeToString(encrypt);
          return encodeEncrypt;

      }catch (UnsupportedEncodingException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException
              | BadPaddingException | IllegalBlockSizeException e){
          logger.error("Exception Encrypt : "+ e);
          return null;
        }
    }
    public String decryptData (String encryptedData,String key )  {
        try {
            //Convert String Key to SecretKey
            byte[] decodeKey = Base64Utils.decodeFromString(key);
            SecretKey secretKey = new SecretKeySpec(decodeKey, 0, decodeKey.length, EncryptionType);
            //Decode encryptedData
            byte[] decodeEncryptedData = Base64Utils.decodeFromString(encryptedData);
            logger.info("DATA BEFORE DECRYPT : "+ key+encryptedData);
            //decryptData
            Cipher cipher = Cipher.getInstance(EncryptionType);
            cipher.init(Cipher.DECRYPT_MODE,secretKey);
            String result = new String (cipher.doFinal(decodeEncryptedData));
            logger.info("DecryptData Result : "+ result);
            return result;
        }catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException |BadPaddingException |IllegalBlockSizeException e){
            logger.error("Exception DecryptData : "+ e);
            return null;
        }
    }
}
