package com.myproject.Hello.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myproject.Hello.Models.Data.Key;
import com.myproject.Hello.Models.Data.User;
import com.myproject.Hello.Models.Dto.Register.RegisterRq;
import com.myproject.Hello.Models.Dto.Register.RegisterRs;
import com.myproject.Hello.Repository.KeyRepository;
import com.myproject.Hello.Repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.service.spi.ServiceException;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.util.Random;


@Service
@Slf4j
public class Register {
    Logger logger = Logger.getLogger(Register.class.getName());


    RegisterRs responseReg = new RegisterRs();


    @Autowired
    UserRepository userJpa;
    @Autowired
    KeyRepository keyJPA;



    Gson gson = new Gson();

    public void insertKey(String username,String key )throws HibernateException{
        try {
            logger.info("### Start method insertKey : ");
            Key dataKey = new Key();
            logger.info("data request :     "+username+" randomKey : "+key);
            dataKey = keyJPA.findByUsername(username);
            String js = gson.toJson(dataKey);
            logger.info("data Key Found : "+ js);
            if (dataKey == null) {
                Key add = new Key();
                add.setUsername(username);
                add.setKey(key);
                keyJPA.save(add);
            }else {
                dataKey.setKey(key);
                keyJPA.save(dataKey);
            }

        }catch (HibernateException e){
            logger.error("insert Key error cause : "+ e);
        }

    }
    public RegisterRs getRegister(RegisterRq req)   {
        logger.info("### Starting excute method getRegister");
        String username = req.getUsername().toLowerCase();
        req.setUsername(username);
        String password = null;
        AES_S aes_s = new AES_S();
        RegisterRs responseReg = new RegisterRs();
        boolean checkUsername = checkUsernameisNotReady(username);

    try {
        String randomKey = aes_s.GenerateKey();
        aes_s.Encrypt(req.getPassword(),randomKey );

         password = aes_s.Encrypt(req.getPassword(),randomKey );
         req.setPassword(password);


            boolean result;

            String jsonString =gson.toJson(req);

            //encrypt password

            logger.info("Request :  " + jsonString);


//            user = userJpa.findByUsername(username);
//            jsonString = gson.toJson(user);
//            logger.info("Data Found : "+ jsonString);
        if (checkUsername) {
            User tambah = new User();
            tambah.setUsername(username);
            tambah.setPassword(password);
            result = insertData(tambah);
            if (result==true){
                insertKey(username,randomKey);
                responseReg.setDescription("Succes Register");
                responseReg.setCode("00");
            }else if (result==false){
                responseReg.setDescription("DATA GAGAL DI SIMPAN!");
                responseReg.setCode("02");
            }
        } else{
                responseReg.setCode("01");
                responseReg.setDescription("FAILED USER IS ALREADY");
            }

        } catch (HibernateException | NoSuchAlgorithmException e) {
            logger.error("Error Exception getRegister : " + e);
            responseReg.setCode("500");
            responseReg.setDescription(e.toString());
            logger.error("ERROR CAUSE : " +e );
        }
        String jsonString = gson.toJson(responseReg);
        logger.info(jsonString  );
        return responseReg;
    }


    public RegisterRs DeleteUser(String username) {
        logger.info("###  Start Service Excute Method Delete User  ");
        String jsonString = username;
        try {
            logger.info("data request : " + username);
            User user = new User();
            user = userJpa.findByUsername(username);
            logger.info("find Data by Username!");
            if (user != null) {

                userJpa.delete(user);
                responseReg.setCode("00");
                responseReg.setDescription("Deleted Data is Success");

            }
            if (user == null) {
                responseReg.setCode("02");
                responseReg.setDescription("Data isNot Found");
            }
        } catch (ServiceException e) {
            logger.error("FAILED USING JPA OR SYSTEM CAUSE : " + e);
            responseReg.setCode("500");
            responseReg.setDescription(e.toString());
        }
        logger.info("RESULT DELETED : " + responseReg);
        logger.info("### END PROCESS ");

        return responseReg;
    }

//    public RegisterRs updateUser(RegisterRq req) {
//        logger.info("### Starting excute method updateUser");
//        String username = req.getUsername();
//        String password = req.getPassword();
//        boolean passwordNull = false;
//        if(password.isBlank()){
//            passwordNull = true;
//        }
//        RegisterRs response = new RegisterRs();
//        AES_S aes_s = new AES_S();
//
//        try {
//            username = req.getUsername().toLowerCase();
//            if (passwordNull==false){
//                String randomKey = aes_s.GenerateKey();
//                String newPassword = aes_s.Encrypt(req.getPassword(), randomKey);
//            }
//
//            boolean result = false;
//            req.setPassword(password);
//
//            String jsonString = gson.toJson(req);
//
//            //encrypt password
//
//            logger.info("Request :  " + jsonString);
//
//            User user = new User();
//            user = userJpa.findByUsername(username);
//            jsonString = gson.toJson(user);
//            logger.info("Data Found : " + jsonString);
//          if (user == null){
//              response.setCode("01");
//              response.setDescription("User tidak ditemukan ");
//          }else {
//              if (checkUsernameisAlready(req.g))
//              if (passwordNull==false){
//                  String randomKey = aes_s.GenerateKey();
//                  String newPassword = aes_s.Encrypt(req.getPassword(), randomKey);
//                  username = req.getUsername();
//                  user.setUsername(username);
//                  user.setPassword(newPassword);
//              }
//
//
//          }
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//    }
        public boolean insertData(User user){
        if (user == null){
            logger.info("insert Data Gagal");
            return false;
        }else {
            logger.info("insertData Berhasil!");
           userJpa.save(user);
           return true;
        }
    }
    public String randomKey(){

        byte[] array = new byte[16]; // length is bounded by 7
        new Random().nextBytes(array);
        String encode = new String(array, Charset.forName("UTF-8"));
        return encode;
    }
    public Boolean checkUsernameisNotReady(String username){
        User user = new User();
        user = userJpa.findByUsername(username);
        String jsonString = gson.toJson(user);
        logger.info("DATA FOUND : "+ jsonString );
        if (user==null){
            return true;
        }else {
            return false;
        }
    }

}
