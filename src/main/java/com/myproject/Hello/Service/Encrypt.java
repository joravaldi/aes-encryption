package com.myproject.Hello.Service;

import com.google.gson.Gson;
import org.jboss.logging.Logger;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Encrypt {
    Logger logger = Logger.getLogger(Encrypt.class.getName());
    public  String encryptPassword(String password,String salt){
        logger.info("##### STARTING GENERATE DATA DATA ! ");

        String jsonString = new Gson().toJson(password + salt);

        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes(StandardCharsets.UTF_8));
            byte [] bytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (int i = 0 ; i< bytes.length; i++){
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100,16).substring(1));
            }
            generatedPassword = sb.toString();

        } catch (NoSuchAlgorithmException e) {
           logger.error("#### FAILED GENERATE PASSWORD CAUSE : " + e);
        }
        logger.info(" result generated password : " + generatedPassword);
        logger.info("#### END EXCUTE METHOD");
        return generatedPassword;
    }

}
