package com.myproject.Hello.Service;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.google.gson.Gson;
import com.myproject.Hello.Models.Data.Key;
import com.myproject.Hello.Models.Data.User;
import com.myproject.Hello.Models.Dto.Login.LoginRq;
import com.myproject.Hello.Models.Dto.Login.LoginRs;
import com.myproject.Hello.Models.Dto.Register.RegisterRs;
import com.myproject.Hello.Repository.KeyRepository;
import com.myproject.Hello.Repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

@Service
@Slf4j
public class UserService {
  private  static final Logger logger = Logger.getLogger(UserService.class);
    //RegisterRs responseReg= new RegisterRs();

    @Autowired
    UserRepository userJpa;
    @Autowired
    KeyRepository keyJPA;

    private String username;
    private String password;
    Gson gson = new Gson();

    public LoginRs getCheckLogin(LoginRq req) throws Exception {
//        logger.info(" ### STARTING CHECK LOGIN ");
//        String jsonStringRq = new Gson().toJson(req);
//        logger.info("REQUEST PARAM : " + jsonStringRq);
//        username = req.getUsername();
//        password = req.getPassword();
//        LoginRs response = new LoginRs();
//        User user = new User();
//
//
//        user = userJpa.findByUsername(username);
//        String password =  user.getPassword();
//
//        if (user == null) {
//            response.setCode("01");
//            response.setDescription("Username is Not Found");
//        }if (user!=null){
//            if (decrypt.equals(req.getPassword())){
//                response.setCode("00");
//                response.setDescription("LOGIN SUCCESS");
//            }else {
//                response.setCode("01");
//                response.setDescription("Password is Not Match");
//            }
//        }
//        String jsonStringRs = new Gson().toJson(response);
//        logger.info("Result : " + response);
//        logger.info(" #####END PROCESS");
//        return response;
        logger.info("### Start Excute Method GetCheckLogin ");

        LoginRs response = new LoginRs();
        String jsonReq = gson.toJson(req);
        logger.info("REQUEST PARAM : "+ jsonReq);
        User user = new User();
        user = userJpa.findByUsername(req.getUsername());
        String jsonString = gson.toJson(user);
        logger.info("STATUS DATA FOUND : " + jsonString);
        if (user==null){
            response.setCode("01");
            response.setDescription("User tidak ditemukan");
        }else if (user!=null){
                String passwordDecrypt = getDataPass(user.getUsername());
                 String password = req.getPassword();
            logger.info("Password Decrypt : "+ passwordDecrypt+" request username : "+req.getUsername() + " user : "+user.getUsername());
                if (passwordDecrypt.equals(password) && user.getUsername().equalsIgnoreCase(req.getUsername())){
                    response.setCode("00");
                    response.setDescription("LOGIN SUCCESS");
                }else {
                    response.setDescription("LOGIN GAGAL PASSWORD DAN USERNAME TIDAK TEPAT");
                    response.setCode("02");
                }
        }
        logger.info("Result response : "+ response);
        return response;
    }

    public String getDataPass (String username) throws Exception {
       logger.info("#### Starting Method getData pass");
       String passwordDecrypt= null;
      try {
//          Key key = new Key();
//
//        key = keyJPA.findByUsername(username);
//        String kunci = key.getKey();
//       // String decode = new String (Base64Utils.decodeFromString(kunci));
//        logger.info("key before decode : "+ kunci);
//      //  logger.info("Key after decode : " + decode);
//        User user = userJpa.findByUsername(username);
//        AES aes = new AES(key.getKey());
//       password = aes.decrypt(user.getPassword());
//        logger.info("RESULT username : " + user + " , password : "+password);
          Key key = new Key();
          User user = new User();
          AES_S aes = new AES_S();
          //get key
          key = keyJPA.findByUsername(username);
          String kunci =  key.getKey();

          //getUser
          user = userJpa.findByUsername(username);
          //String decrypt;
          passwordDecrypt = aes.decryptData(user.getPassword(),kunci);
          logger.info("Result GetData Password: "+passwordDecrypt);


      }catch (Exception e){
          logger.error("Exception pass Failed Cause : "+ e);
        }
      logger.info("### END PROCESS GETDATA PASS");
      return  passwordDecrypt;
    }
}
