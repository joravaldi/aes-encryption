package com.myproject.Hello.Controller;

import com.myproject.Hello.Models.Dto.Login.LoginRq;
import com.myproject.Hello.Models.Dto.Login.LoginRs;
import com.myproject.Hello.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/Login")

public class UserLogin {
@Autowired
UserService userLogin;
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/checkLogin", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public LoginRs getCheckLogin (@RequestBody(required = true) LoginRq requestLogin, HttpServletRequest request, HttpServletResponse response) throws Exception {
        return  userLogin.getCheckLogin(requestLogin);
    }
}
