package com.myproject.Hello.Controller;

import com.myproject.Hello.Models.Dto.Login.LoginRq;
import com.myproject.Hello.Models.Dto.Login.LoginRs;
import com.myproject.Hello.Models.Dto.Register.RegisterRq;
import com.myproject.Hello.Models.Dto.Register.RegisterRs;
import com.myproject.Hello.Service.Encrypt;
import com.myproject.Hello.Service.Register;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/Register")
public class UserRegister {
    @Autowired
    Register svcRegister;
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/Add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RegisterRs register (@RequestBody(required = true) RegisterRq requestLoginReg, HttpServletRequest request, HttpServletResponse response) throws Exception {
        return  svcRegister.getRegister(requestLoginReg);
    }
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PostMapping(value = "/Add2", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RegisterRs getRegister (@RequestBody(required = true) HttpServletRequest request,HttpServletResponse response,
                                   RegisterRq registerRq) throws Exception {

          return svcRegister.getRegister(registerRq);
    }
//    @ResponseStatus(HttpStatus.OK)
//    @ResponseBody
//    @PostMapping(value = "/Update",produces = MediaType.APPLICATION_JSON_VALUE, consumes =  MediaType.APPLICATION_JSON_VALUE)
//    public RegisterRs getUpdate (@RequestBody(required = true) HttpServletResponse response,
//                                 HttpServletRequest request, RegisterRq registerRq){
//        return  svcRegister.updateUser(registerRq);
//    }
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PostMapping(value = "/Delete",produces = MediaType.APPLICATION_JSON_VALUE, consumes =  MediaType.APPLICATION_JSON_VALUE)
    public RegisterRs getDeleted (@RequestBody(required = true) HttpServletResponse response,
                                  HttpServletRequest request, RegisterRq registerRq){
        String username = registerRq.getUsername();
        return  svcRegister.DeleteUser(username);
    }
}
